# Spletno Programiranje - Skupina 32
# Doumentacija za projekt Knjižnica

Odločili smo se narediti aplikacijo za nudenje informacijske podpore knjižnici.
Knjižnica se imenuje Super knjižnica in se nahaja v prostorih FRI v Ljubljani.

Spletna stran knjižnice ponuja vse kar bi morala spletna stran sodobne knjižnice nuditi. Omogoča registracijo novih uporabnikov ter prijavo obstoječih. Uporabnikom omogoča brskanje po razpoložljivih knjigah, izpis podrobnosti izbrane knjige ter dodajanje komentarjev. Administratorju pa omogoča tudi dodajanje novih knjig v aplikacijo, urejanje obstoječih knjig, omogoča mu tudi pregled nad statistiko izposojenih knjig ter uporabnikov. Poleg tega ima knjižnica že stran z opisom ter kontaktom.
Aplikacija je v celoti zgrajena v html jeziku, oblikovni slogi so v celoti definirani v svoji .css datoteki z imenom style.css. Od zunanjih knjižnic uporabljamo bootstrap, kar nam omogoča, da se naša aplikacija prilagaja zaslonom različnih velikosti in orientacij.

## Podprte naprave

Naša aplikacija deluje na naslednjih treh napravah:

* osebni računalnik s full HD monitorjem (1920x1080)
* tablica iPad Pro (1024x1366)
* telefon v pokončni usmeritvi (Mobile L) (425x850)

## Preverjanje vnosa

Vsi obrazci imajo preverjanje na strani odjemalca z obveznimi polji:

* Prijava (root)
* Registracija (/registracija)
* Dodajanje Knjige (/dodajanjeElementa)
* Urejanje knjige (/urejanjeElementaAdmin/{ID})

Pri Registraciji pa imamo implementirano še dodatno preverjanje: Polji "Geslo" ter "Ponovi geslo" morata biti enaki.

## Heroku

Povezava na Heroku se nahaja [tukaj](https://sp-32.herokuapp.com/).


## Navodila za zagon aplikacije
Lokalno bazo poženemo tako, da se premaknemo v pravi direktorij in poženemo ukaz mongod.

```
cd sp_32/mongodb
./mongod
```

Aplikacijo v okolju Cloud9 poženemo tako, da se premaknemo v direktorij "_library", namestimo vse potrebne pakete ter poženemo Node server.

```
cd sp_32/_library
npm install
npm start
```


## Zaslonske maske

Vse zaslonske maske imajo enak navigacijski meni na vrhu strani, kjer uporabnik in administrator lahko navigirata po aplikaciji. Premakne se lahko na domačo stran, na stran za dodajanje gradiva, stran za iskanje po materialu, pregled nad izposojenimi knjigami, stran za statistiko, stran z informacijami o knjižnici ter prijava oziroma registracija uporabnika, oziroma odjava, če je uporabnik že prijavljen v sistem.


### Zaslonska maska za prijavo uporabnika
https://bitbucket.org/JoeY34kaze/sp_32/src/master/docs/prijava.html

Na strani za prijavo tako uporabnik, kot tudi administrator vstopi v našo spletno aplikacijo. Stran vsebuje vnosna polja za elektronski naslov ter geslo. Prijava se izvede ob kliku na zeleni gumb z napisom “Prijava”.

### Zaslonska maska za registracijo uporabnika
https://bitbucket.org/JoeY34kaze/sp_32/src/master/docs/registracija.html

Če uporabnik še nima ustvarjenega računa v aplikaciji, ga ustvari tako, da na tej strani pravilno vnese svoje podatke v vnosna polja. Registracija se izvede ob kliku na zeleni gumb z napisom “Registracija”, ki se nahaja pod obrazcem.

### Zaslonska maska za informacije o knjižnici ter kontakt
https://bitbucket.org/JoeY34kaze/sp_32/src/master/docs/about.html

Tukaj je kratek opis knjižnice ter njeni osnovni kontaktni podatki, kot je naslov in telefon. Uporabljen bo Google Maps za interaktiven prikaz lokacije knjižnice.

### Zaslonska maska za domačo stran (administrator)
https://bitbucket.org/JoeY34kaze/sp_32/src/master/docs/domov_admin.html

To je domača stran, ki se prikaže ko se administrator prijavi v sistem. Ima pregled nad vsemi knjigami v sistemu ter administratorju ponuja možnost da določeno knjigo izbriše iz ponudbe ali pa uredi njene podatke. Vsako izmed knjig lahko tudi podrobno pregleda (podrobnosti ter komentarje).

### Zaslonska maska za domačo stran (uporabnik)
https://bitbucket.org/JoeY34kaze/sp_32/src/master/docs/domov_prijavljen.html

To je domača stran, ki se prikaže ko se uporabnik prijavi v sistem. Podobno kot administrator ima uporabnik pregled nad vsemi knjigami v sistemu. Vsako izmed knjig lahko tudi podrobno pregleda (podrobnosti ter komentarje). Uporabnik ima možnost knjigo rezervirati.

### Zaslonska maska za pregled knjige (administrator)
https://bitbucket.org/JoeY34kaze/sp_32/src/master/docs/knjiga_detajli_admin.html

Ta stran se odpre ko administrator klikne na eno izmed knjig. Prikazuje vse podatke o knjigi ter njene komentarje. Administratorju nudi možnost podatke o knjigi urediti ali pa knjigo odstraniti.


### Zaslonska maska za pregled knjige (uporabnik)
https://bitbucket.org/JoeY34kaze/sp_32/src/master/docs/knjiga_detajli.html

Stran je podobna kot stran za pregled knjige administratorja, le da uporabniku namesto urejanja ali brisanja knjige omogoča njeno rezervacijo. Uporabnik si lahko tudi ogleda predogled knjige (uporabljen je Google Books API).

### Zaslonska maska za predogled knjige
https://bitbucket.org/JoeY34kaze/sp_32/src/master/docs/google_book.html

Uporabnik si lahko ogleda predogled knjige in se na podlagi tega odloči ali mu je knjiga zanimiva ter ali si bi jo želel izposoditi. Uporabljen je Google Books API. 

### Zaslonska maska za pregled izposojenih ter rezerviranih knjig (uporabnik)
https://bitbucket.org/JoeY34kaze/sp_32/src/master/docs/izposojene_prijavljen.html

Stran uporabniku omogoča pregled nad izposojenimi knjigami. Za vsako knjigo so navedeni osnovni podatki ter rok, do katere mora biti knjiga vrnjena. Uporabniku prav tako omogoča pregled nad knjigami, ki so rezervirane. S klikom na posamezno knjigo se prikažejo podrobni podatki o knjigi. 

### Zaslonska maska za pregled izposojenih ter rezerviranih knjig (administrator)
https://bitbucket.org/JoeY34kaze/sp_32/src/master/docs/izposojene_admin.html


Stran je podobna kot stran za uporabnike, le da administrator vidi tudi podatke o osebi, ki si je knigo izposodila ali rezervirala. 

### Zaslonska maska za dodajanje nove knjige
https://bitbucket.org/JoeY34kaze/sp_32/src/master/docs/dodajanje_elementa.html

Na tej strani lahko administrator doda novo knjigo (ali kak drug medij - CD, kaseto, revijo itd.) v bazo razpoložljivih knjig. Pri tem mora vnesti podatke o dodajanem gradivu (naslov, avtorja, datum izdaje itd.). Dodajanje administrator potrdi s klikom na gumb na dnu strani, gradivo pa je po dodajanju vidno vsem uporabnikom.

### Zaslonska maska za urejanje knjige
https://bitbucket.org/JoeY34kaze/sp_32/src/master/docs/urejanje_elementa.html

Stran je podobna tisti za dodajanje nove knjige, le da se ob urejanju obstoječe knjige podatki avtomatsko izpolnijo, da jih lahko administrator spremeni po želji. Na dnu strani ima administrator možnost potrditi urejanje s klikom na gumb “Uredi” ali knjgo izbrisati iz sistema s klikom na gumb “Izbriši”


### Zaslonska maska za pregled statistike o uporabnikih ter izposoji knjig
https://bitbucket.org/JoeY34kaze/sp_32/src/master/docs/statistika.html

Ta stran je na voljo administratorju, da ima pregled nad statistiko izposojenih knjig. Na voljo ima podatke o najbolj aktivnem uporabniku (uporabniku, ki si je izposodil največ knjig) ter podatke o uporabniku, ki ima trenutno izposojenih največ knjig. Ogleda si lahko tudi zgodovino izposojenih knjig za posameznega uporabnika. 

### Zaslonska maska za prikaz najbolj priljubljenih knjig meseca
https://bitbucket.org/JoeY34kaze/sp_32/src/master/docs/top_izposojene_uporabnik.html

Na tej strani so prikazane knjige, ki so bile v zadnjem mesecu najpogosteje izposojene. Če je knjiga na voljo, ima uporabnik možnost knjigo rezervirati s klikom na gumb “rezerviraj”. S klikom na posamezno knjigo se uporabniku izpišejo podrobnosti knjige ter komentarji drugih uporabnikov.


### Zaslonska maska za poizvedovanje po gradivu ( Uporabnik )
https://bitbucket.org/JoeY34kaze/sp_32/src/master/docs/iskanje.html

Stran vsebuje dva polja, preko katerih izvedemo željeno poizvedbo. V prvem izberemo po čem želimo izvesti poizvedbo (avtorju, naslovu knjige, žanru, letu izdaje ali jeziku), nato pa v vnosno polje poleg naše izbire vpišemo niz po katerem naj se izvede poizvedba. Iskanje se izvede z klikom na zeleni gumb pod vnosnim poljem z imenom “ Išči “. Rezultati poizvedbe se izpišejo pod gumbom, kjer vsaka vrstica predstavlja en zadetek poizvedbe. V vrstici so predstavljene najpomembnejše informacije, ob kliku na vrstico pa se odpre stran za pregled knjige.

### Zaslonska maska za poizvedovanje po gradivu ( Administrator )
https://bitbucket.org/JoeY34kaze/sp_32/src/master/docs/iskanje_admin.html

Stran je z manjšim dodatkom enaka kot pri uporabniku. Razlika je le v tem da ima administrator dodaten gumb, ki mu omogoča poleg pregleda elementa, tudi urejanje ali pa tudi izbri le-tega.