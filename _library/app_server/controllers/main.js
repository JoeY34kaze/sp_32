var request = require('request');
var apiParametri = {
  streznik: 'http://localhost:' + process.env.PORT
};
if (process.env.NODE_ENV === 'production') {
  //apiParametri.streznik = 'https://????nas heroku streznik//////////';
}

module.exports.domovAdmin = function(req, res) {
  var pot = '/api/objekti';
  var parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'GET',
    json: {}
  };
  request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
      prikaziDomovAdmin(req, res, vsebina);
    }
  );
};

module.exports.domovUser = function(req, res) {
  var pot = '/api/objekti';
  var parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'GET',
    json: {}
  };
  request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
      prikaziDomovUser(req, res, vsebina);
    }
  );
};

module.exports.prijava = function(req, res) {
  res.render('prijava', { title: 'Knjižnica - Prijava' });
};

module.exports.registracija = function(req, res) {
  res.render('registracija', { title: 'Knjižnica - Registracija' });
};

module.exports.about = function(req, res) {
  res.render('about', { title: 'Knjižnica - Registracija' });
};

module.exports.dodajanjeElementa = function(req, res, vsebina) {
  res.render('dodajanjeElementa', { Objekti: vsebina });
};

module.exports.dodajEl = function(req, res, vsebina) {
  var pot = "/api/objekti";
    var parametriZahteve = {
      url: apiParametri.streznik + pot,
      method: 'POST',
      json: {
        naslov: req.body.naslov,
        avtor: req.body.avtor,
        zanr: req.body.zanr,
        medij: req.body.medij,
        datumIzdaje: req.body.datumIzdaje,
        jezik: req.body.jezik,
        gBookId: req.body.gBookId
      }
    };
    request(
      parametriZahteve,
      function(napaka, odgovor, vsebina) {
      prikaziDetajleAdmin(req, res, vsebina);
    }
    );
};

module.exports.iskanjeAdmin = function(req, res) {
  var pot = "/api/iskanje";
  var parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'GET',
    json: {},
    qs: {
      param: req.query.param,
      val: req.query.val
    }
  };
  console.log(parametriZahteve)
  request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
      prikaziIskanjeAdmin(req, res, vsebina);
    }
  );
};

module.exports.iskanjeUser = function(req, res) {
  var pot = "/api/iskanje";
  var parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'GET',
    json: {},
    qs: {
      param: req.query.param,
      val: req.query.val
    }
  };
  request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
      prikaziIskanjeUser(req, res, vsebina);
    }
  );
};

module.exports.izposojeneAdmin = function(req, res) {
  var pot = '/api/iskanje';
  var parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'GET',
    json: {},
    qs: {
      param: "status",
      val: false
    }
  };
  request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
      prikaziIzposojeneAdmin(req, res, vsebina);
    }
  );
};

module.exports.izposojeneUser = function(req, res) {
  var pot = '/api/iskanje';
  var parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'GET',
    json: {},
    qs: {
      param: "status",
      val: false
    }
  };
  request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
      prikaziIzposojeneUser(req, res, vsebina);
    }
  );
};

module.exports.statistikaAdmin = function(req, res) {
  var pot = '/api/userTopIzposoj';
  var parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'GET',
    json: {}
  };
  request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
      
      var pot = '/api/userTopStKnjig';
      var parametriZahteve = {
        url: apiParametri.streznik + pot,
        method: 'GET',
        json: {}
      };
      request(
        parametriZahteve,
        function(napaka, odgovor, vsebina2) {
          prikaziStatistikaAdmin(req, res, vsebina.concat(vsebina2));
        }
      );
    }
  );
};

module.exports.topIzposojeneAdmin = function(req, res) {
  var pot = '/api/topObjekti';
  var parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'GET',
    json: {}
  };
  request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
      prikaziTopIzposojeneAdmin(req, res, vsebina);
    }
  );
};

module.exports.topIzposojeneUser = function(req, res) {
  var pot = '/api/topObjekti';
  var parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'GET',
    json: {}
  };
  request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
      prikaziTopIzposojeneUser(req, res, vsebina);
    }
  );
};

module.exports.izbrisiAdmin = function(req, res) {
  var pot = '/api/objekti/' + req.params.id;
  var parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'DELETE',
    json: {}
  };
  request(
    parametriZahteve
  );
  var pot = '/api/objekti';
  var parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'GET',
    json: {}
  };
  request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
      prikaziDomovAdmin(req, res, vsebina);
    }
  );
};

module.exports.detailAdmin = function(req, res) {
  var pot = '/api/objekti/' + req.params.id;
  var parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'GET',
    json: {}
  };
  request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
      prikaziDetajleAdmin(req, res, vsebina);
    }
  );
};

module.exports.urejanjeElementaAdmin = function(req, res) {
  //console.log(dataJSON.Objekti.find(item => item.id == req.params.id)); //stackoverflow magic, i'm not questioning it
  var pot = '/api/objekti/' + req.params.id;
  var parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'GET',
    json: {}
  };
  request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
      //console.log(vsebina);
      urejanjeElementaAdmin(req, res, vsebina);
    }
  );
};

module.exports.UrediEl = function(req, res, vsebina) {
  var pot = "/api/objekti/" + req.params.id;
    var parametriZahteve = {
      url: apiParametri.streznik + pot,
      method: 'PATCH',
      json: {
        naslov: req.body.naslov,
        avtor: req.body.avtor,
        zanr: req.body.zanr,
        medij: req.body.medij,
        datumIzdaje: req.body.datumIzdaje,
        jezik: req.body.jezik,
        gBookId: req.body.gBookId
      }
    };
    request(
      parametriZahteve,
      function(napaka, odgovor, vsebina) {
      res.redirect('/detailAdmin/' + req.params.id);
    }
    );
};

module.exports.detailUser = function(req, res) {
  //console.log(dataJSON.Objekti.find(item => item.id == req.params.id)); //stackoverflow magic, i'm not questioning it
  var pot = '/api/objekti/' + req.params.id;
  var parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'GET',
    json: {}
  };
  request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
      prikaziDetajleUser(req, res, vsebina);
    }
  );
};

module.exports.googleBookPage = function(req, res) {
  // res.render('googleBook', { title: 'Google Knjiga predogled', obj: dataJSON.Objekti.find(item => item.id == req.params.id) });
  var pot = '/api/objekti/' + req.params.id;
  var parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'GET',
    json: {}
  };
  request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
      prikaziGoogleBooks(req, res, vsebina);
    }
  );
};

module.exports.shraniKomentar = function(req, res) {
  var idObjekta = req.params.id;
  var pot = '/api/objekti/' + idObjekta + '/komentarji';
  var posredovaniPodatki = {
    avtor: req.body.avtor,
    komentar: req.body.komentar
  };
  var parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'POST',
    json: posredovaniPodatki
  };
  request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
      res.redirect('/detailAdmin/' + idObjekta);
    }
  );
};

module.exports.shraniKomentarUser = function(req, res) {
  var idObjekta = req.params.id;
  var pot = '/api/objekti/' + idObjekta + '/komentarji';
  var posredovaniPodatki = {
    avtor: req.body.avtor,
    komentar: req.body.komentar
  };
  var parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'POST',
    json: posredovaniPodatki
  };
  request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
      res.redirect('/detailUser/' + idObjekta);
    }
  );
};

module.exports.izbrisiKomentar = function(req, res) {
  var idObjekta = req.params.id;
  var idKomentarja = req.params.idKomentarja;
  var pot = '/api/objekti/' + idObjekta + '/komentarji/' + idKomentarja;
  var parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'DELETE',
    json: {}
  };
  request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
      res.redirect('/detailAdmin/' + idObjekta);
    }
  );
};


module.exports.db = function(req, res) {
  res.render('db', { title: 'Baza' });
};


module.exports.dbZacetniPodatki = function(req, res) {
  var pot = "/api/db/zacetniPodatki";
    var parametriZahteve = {
      url: apiParametri.streznik + pot,
      method: 'POST', 
      json: {}
    };
    request(
      parametriZahteve,
      function(napaka, odgovor, vsebina) {
        res.redirect('/db');
      }
    );
};

module.exports.dbIzbrisi = function(req, res) {
  var pot = '/api/db/izbrisi';
  var parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'DELETE',
    json: {}
  };
  request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
      res.redirect('/db');
    }
  );
};

module.exports.izposoja = function(req, res) {
  var pot = '/api/rezervacija';
  var parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'POST',
    json: {
      uporabnikId: req.params.idUporabnika, 
      knjigaId: req.params.idKnjige
    }
  };
  request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
      // Potrebno obvestit uporabnika da je bila rezervacija uspešna. To se bo naredilo mogoče kasneje v Angularju.
      res.redirect('/detailUser/' + req.params.idKnjige);
    }
  );
};



/*------------------------------ API DOSTOPI ------------------------------ */

var prikaziDomovUser = function(req, res, vsebina) {
  res.render('domovUser', { Objekti: vsebina });
};

var prikaziDomovAdmin = function(req, res, vsebina) {
  res.render('domovAdmin', { Objekti: vsebina });
};

var prikaziDetajleUser = function(req, res, vsebina) { //objekt je samo eden
  res.render('detailUser', { objekt: vsebina });
};

var prikaziGoogleBooks = function(req, res, vsebina) { //objekt je samo eden
  res.render('googleBook', { objekt: vsebina });
};

var prikaziDetajleAdmin = function(req, res, vsebina) {
  res.render('detailAdmin', { objekt: vsebina });
};

var urejanjeElementaAdmin = function(req, res, vsebina) {
  res.render('urejanjeElementaAdmin', { objekt: vsebina });
};

var prikaziIskanjeAdmin = function(req, res, vsebina) {
  res.render('iskanjeAdmin', { Objekti: vsebina });
};

var prikaziIskanjeUser = function(req, res, vsebina) {
  res.render('iskanjeUser', { Objekti: vsebina });
};

var prikaziIzposojeneAdmin = function(req, res, vsebina) {
  res.render('izposojeneAdmin', { Objekti: vsebina });
};

var prikaziIzposojeneUser = function(req, res, vsebina) {
  res.render('izposojeneUser', { Objekti: vsebina });
};

var prikaziStatistikaAdmin = function(req, res, vsebina) {
  res.render('statistikaAdmin', { Objekti: vsebina });
};

var prikaziTopIzposojeneAdmin = function(req, res, vsebina) {
  res.render('topIzposojeneAdmin', { Objekti: vsebina });
};

var prikaziTopIzposojeneUser = function(req, res, vsebina) {
  res.render('topIzposojeneUser', { Objekti: vsebina });
};