var express = require('express');
var router = express.Router();
var ctrlMain = require('../controllers/main');
var ctrlOstalo = require('../controllers/ostalo');

/* GET home page. */
router.get('/', ctrlOstalo.angularApp)//-----------------------SPA
router.get('/domovUser',ctrlOstalo.angularApp)//-----------------------SPA
router.get('/domovAdmin',ctrlMain.domovAdmin)
router.get('/registracija',ctrlMain.registracija)
router.get('/about', ctrlMain.about);



router.get('/detailUser/:id',ctrlOstalo.angularApp)//-----------------------SPA
router.get('/detailAdmin/:id', ctrlMain.detailAdmin);

router.post('/detailAdmin/:id/komentar', ctrlMain.shraniKomentar);
router.get('/detailAdmin/:id/komentar/:idKomentarja/izbrisi', ctrlMain.izbrisiKomentar);
router.post('/detailUser/:id/komentar', ctrlMain.shraniKomentarUser);



router.get('/dodajanjeElementa/', ctrlMain.dodajanjeElementa);
router.post('/dodajanjeElementa/', ctrlMain.dodajEl);

router.get('/urejanjeElementaAdmin/:id', ctrlMain.urejanjeElementaAdmin);
router.post('/urejanjeElementaAdmin/:id', ctrlMain.UrediEl);

router.get('/izbrisiAdmin/:id', ctrlMain.izbrisiAdmin);



router.get('/iskanjeAdmin/', ctrlMain.iskanjeAdmin);
router.get('/iskanjeUser/', ctrlMain.iskanjeUser);



router.get('/izposojeneAdmin/', ctrlMain.izposojeneAdmin);
router.get('/izposojeneUser/', ctrlMain.izposojeneUser);

router.get('/topIzposojeneUser/', ctrlMain.topIzposojeneUser);
router.get('/topIzposojeneAdmin/', ctrlMain.topIzposojeneAdmin);

router.get('/statistikaAdmin/', ctrlMain.statistikaAdmin);



router.get('/googleBook/:id',ctrlMain.googleBookPage);

router.get('/db', ctrlMain.db);
router.get('/dbZacetniPodatki', ctrlMain.dbZacetniPodatki);
router.get('/dbIzbrisi', ctrlMain.dbIzbrisi);


router.get('/rezervacija/:idKnjige/:idUporabnika', ctrlMain.izposoja);



module.exports = router;