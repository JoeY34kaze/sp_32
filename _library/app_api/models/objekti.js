var mongoose = require('mongoose');

var komentarjiShema = new mongoose.Schema({
  avtor: String,
  besediloKomentarja: String
});

var objektiShema=new mongoose.Schema({
    naslov:{type:String, required: true},
    avtor:String,
    ocena:{type:Number, "default": 0, min: 0,max: 5},
    status:{type:Boolean, "default": true},
    medij:String,
    zanr:String,
    datumIzdaje:{type:Date},
    jezik:String, 
    nIzposojena:{type:Number, required:true, "default": 0},
    gBookId:String,
    komentarji: [komentarjiShema]
});

var usersShema=new mongoose.Schema({
    upIme:{type:String, required: true},
    ime:{type:String, required: true},
    priimek:{type:String, required: true},
    izposojenihTrenutno:Number, 
    stIzposoj:Number, 
    izposojeneTrenutno: [objektiShema], 
    zgodovina: [objektiShema]
});


var izposojaShema=new mongoose.Schema({
    uporabnikId:{type:String, required: true},
    knjigaId:{type:String, required: true},
    datumRezervacije:{type:Date, default: Date.now}, 
    datumIzposoje:{type:Date}, 
    datumVrnitve:{type:Date}
});




mongoose.model('Objekt',objektiShema);
mongoose.model('User',usersShema);
mongoose.model('Izposoja',izposojaShema);



//iz prosojnic. im not comfortable about this tho
//mongoose sam avtomatsko doda s na koncu. tko da dejansko bo tabeli ime "objekts"...
//do baze dostopaš tkole
/*najprej poženeš lokalno bazo
cd ~workspace/mongodb
./mongodb

odpreš nov terminal in poženeš ukaz mongo
odpre se ti ta shell za mongo, zdj si povezan na bazo

če daš
show dbs
vidš da je eni bazi ime library - to je naša baza, prestavš se ke not
use library
show collections - pokaže ti "tabele" od te baze in vidš da je tukej not objekts
db.objekts.find() - vrne vse kar je v tej tabeli/dokumentu

*/




