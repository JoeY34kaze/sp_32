var express = require('express');
var router = express.Router();
var ctrlMain = require('../controllers/main');
var ctrlKomentarji = require('../controllers/komentarji');

/*--------------------------------- KNJIGE --------------------------------- */

router.get('/objekti', ctrlMain.vsiObjekti);
router.get('/objekti/:id',ctrlMain.getObjektById);
router.get('/iskanje',ctrlMain.searchObjekt);
router.post('/objekti', ctrlMain.objektCreate);
router.delete('/objekti/:id', ctrlMain.objektIzbrisi);
router.patch('/objekti/:id', ctrlMain.objektUredi);


// top 5 knjig po številu izposoj
router.get('/topObjekti', ctrlMain.topKnjige);

/*------------------------------- UPORABNIKI ------------------------------- */
  
router.get('/userji', ctrlMain.vsiUserji);
router.post('/userji', ctrlMain.userCreate);
router.delete('/userji/:id', ctrlMain.userDelete);

// uporabnik ki si je izposodil največ knjig
router.get('/userTopIzposoj', ctrlMain.userTopIzposoj);
  
// uporabnik z največ trenutno izposojenimi knjigami
router.get('/userTopStKnjig', ctrlMain.userTopStKnjig);

/*------------------------------- KOMENTARJI ------------------------------- */

router.get('/objekti/:idObjekta/komentarji/:idKomentarja', ctrlKomentarji.getKomentar);
router.post('/objekti/:idObjekta/komentarji', ctrlKomentarji.komentarjiKreiraj);
router.delete('/objekti/:idObjekta/komentarji/:idKomentarja', ctrlKomentarji.komentarjiIzbrisiIzbranega);

/*--------------------------------- OSTALO --------------------------------- */
  
router.post('/db/zacetniPodatki', ctrlMain.dbZacetniPodatki);
router.delete('/db/izbrisi', ctrlMain.dbIzbrisi);


router.post('/rezervacija', ctrlMain.rezervacija);





module.exports = router;






/*router.post('/lokacije', 
  ctrlLokacije.lokacijeKreiraj);
router.get('/lokacije/:idLokacije', 
  ctrlLokacije.lokacijePreberiIzbrano);
router.put('/lokacije/:idLokacije', 
  ctrlLokacije.lokacijePosodobiIzbrano);
router.delete('/lokacije/:idLokacije', 
  ctrlLokacije.lokacijeIzbrisiIzbrano);

 Komentarji 
router.post('/lokacije/:idLokacije/komentarji', 
  ctrlKomentarji.komentarjiKreiraj);
router.get('/lokacije/:idLokacije/komentarji/:idKomentarja', 
  ctrlKomentarji.komentarjiPreberiIzbranega);
router.put('/lokacije/:idLokacije/komentarji/:idKomentarja', 
  ctrlKomentarji.komentarjiPosodobiIzbranega);
router.delete('/lokacije/:idLokacije/komentarji/:idKomentarja', 
  ctrlKomentarji.komentarjiIzbrisiIzbranega);
  */

