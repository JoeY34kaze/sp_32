var mongoose = require('mongoose');
var Objekt = mongoose.model('Objekt');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.komentarjiKreiraj = function(zahteva, odgovor) {
  var idObjekta = zahteva.params.idObjekta;
  if (idObjekta) {
    Objekt
      .findById(idObjekta)
      .select('komentarji')
      .exec(
        function(napaka, obj) {
          if (napaka) {
            vrniJsonOdgovor(odgovor, 400, napaka);
          } else {
            dodajKomentar(zahteva, odgovor, obj);
          }
        }
      );
  } else {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": 
        "Ne najdem objekta, idObjekta je obvezen parameter."
    });
  }
};

var dodajKomentar = function(zahteva, odgovor, obj) {
  if (!obj) {
    vrniJsonOdgovor(odgovor, 404, {
      "sporočilo": "Ne najdem objekta."
    });
  } else {
    obj.komentarji.push({
      avtor: zahteva.body.avtor,
      besediloKomentarja: zahteva.body.komentar
    });

    obj.save(function(napaka, obj) {
      var dodaniKomentar;
      if (napaka) {
        console.log(napaka);
        vrniJsonOdgovor(odgovor, 400, napaka);
      } else {
        dodaniKomentar = obj.komentarji[obj.komentarji.length - 1];
        vrniJsonOdgovor(odgovor, 201, dodaniKomentar);
      }
    });
  }
};

module.exports.komentarjiIzbrisiIzbranega = function(zahteva, odgovor) {
  if (!zahteva.params.idObjekta || !zahteva.params.idKomentarja) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": 
        "Ne najdem objekta oz. komentarja, " + 
        "idObjekta in idKomentarja sta obvezna parametra."
    });
    return;
  }
  Objekt
    .findById(zahteva.params.idObjekta)
    .exec(
      function(napaka, obj) {
        if (!obj) {
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": "Objekta ne najdem."
          });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        }
        if (obj.komentarji && obj.komentarji.length > 0) {
          if (!obj.komentarji.id(zahteva.params.idKomentarja)) {
            vrniJsonOdgovor(odgovor, 404, {
              "sporočilo": "Komentarja ne najdem."
            });
          } else {
            obj.komentarji.id(zahteva.params.idKomentarja).remove();
            obj.save(function(napaka) {
              if (napaka) {
                vrniJsonOdgovor(odgovor, 500, napaka);
              } else {
                vrniJsonOdgovor(odgovor, 204, null);
              }
            });
          }
        } else {
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": "Ni komentarja za brisanje."
          });
        }
      }
    );
};

module.exports.getKomentar = function(zahteva, odgovor) {
  if (!zahteva.params.idObjekta || !zahteva.params.idKomentarja) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": 
        "Ne najdem objekta oz. komentarja, " + 
        "idObjekta in idKomentarja sta obvezna parametra."
    });
    return;
  }
  Objekt
    .findById(zahteva.params.idObjekta)
    .exec(
      function(napaka, obj) {
        if (!obj) {
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": "Objekta ne najdem."
          });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        }
        if (obj.komentarji && obj.komentarji.length > 0) {
          if (!obj.komentarji.id(zahteva.params.idKomentarja)) {
            vrniJsonOdgovor(odgovor, 404, {
              "sporočilo": "Komentarja ne najdem."
            });
          } else {
              obj.save(function(napaka) {
              if (napaka) {
                vrniJsonOdgovor(odgovor, 500, napaka);
              } else {
                vrniJsonOdgovor(odgovor, 200, obj.komentarji.id(zahteva.params.idKomentarja));
              }
          });
          }
        } else {
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": "Ni komentarja."
          });
        }
      }
    );
};