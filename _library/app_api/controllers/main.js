var mongoose = require('mongoose');
var Objekt = mongoose.model('Objekt');
var User = mongoose.model('User');
var Izposoja = mongoose.model('Izposoja');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};


/*--------------------------------- KNJIGE --------------------------------- */

module.exports.vsiObjekti = function(zahteva, odgovor) {
    Objekt.find()
    .exec(function(napaka,objekt2){
        //console.log(JSON.stringify(objekt2, null, "    "));
        vrniJsonOdgovor(odgovor, 200, objekt2);
    });
};

module.exports.searchObjekt = function(zahteva, odgovor) {
    var param = zahteva.query.param;
    var val = zahteva.query.val;
    var query = {};
    query[param] = val;
    Objekt.find(query)
    .exec(function(napaka,obj){
        //console.log(JSON.stringify(objekt2, null, "    "));
        vrniJsonOdgovor(odgovor, 200, obj);
    });
}; 

module.exports.getObjektById=function(zahteva,odgovor){
    console.log(zahteva.params.id);
    
    Objekt
    .findById(zahteva.params.id)
    .exec(function(napaka,obj){
        //console.log(obj);
       vrniJsonOdgovor(odgovor,200,obj); 
    });
}

module.exports.objektCreate = function(zahteva, odgovor) {
  Objekt.create({
    naslov:zahteva.body.naslov,
    avtor:zahteva.body.avtor,
    medij:zahteva.body.medij,
    zanr:zahteva.body.zanr,
    datumIzdaje:zahteva.body.datumIzdaje,
    jezik:zahteva.body.jezik,
    gBookId:zahteva.body.gBookId
  }, function(napaka, obj) {
    if (napaka) {
      vrniJsonOdgovor(odgovor, 400, napaka);
    } else {
      vrniJsonOdgovor(odgovor, 201, obj);
    }
  });
};

module.exports.objektIzbrisi = function(zahteva, odgovor) {
  var id = zahteva.params.id;
  if (id) {
    Objekt
      .findByIdAndRemove(id)
      .exec(
        function(napaka, obj) {
          if (napaka) {
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
          }
          vrniJsonOdgovor(odgovor, 204, null);
        }
      );
  } else {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": 
        "Ne najdem knjige, id je obvezen parameter."
    });
  }
};

module.exports.objektUredi = function(zahteva, odgovor) {
  var id = zahteva.params.id;
  if (id) {
    Objekt.findOneAndUpdate({_id: id}, 
      {
        naslov:zahteva.body.naslov,
        avtor:zahteva.body.avtor,
        medij:zahteva.body.medij,
        zanr:zahteva.body.zanr,
        datumIzdaje:zahteva.body.datumIzdaje,
        jezik:zahteva.body.jezik,
        gBookId:zahteva.body.gBookId
      }
      , function(napaka, obj) {
      if (napaka) {
        vrniJsonOdgovor(odgovor, 400, napaka);
      } else {
        vrniJsonOdgovor(odgovor, 204, null);
      }
    });
  } else {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": 
        "Ne najdem knjige, id je obvezen parameter."
    });
  }
};

module.exports.topKnjige = function(zahteva, odgovor) {
    Objekt.find().sort([['nIzposojena', 'descending']]).limit(5)
    .exec(function(napaka,objekt2){
        //console.log(JSON.stringify(objekt2, null, "    "));
        vrniJsonOdgovor(odgovor, 200, objekt2);
    });
};


/*------------------------------- UPORABNIKI ------------------------------- */


module.exports.vsiUserji = function(zahteva, odgovor) {
    User.find()
    .exec(function(napaka,objekt2){
        //console.log(JSON.stringify(objekt2, null, "    "));
        vrniJsonOdgovor(odgovor, 200, objekt2);
    });
};

module.exports.userTopStKnjig = function(zahteva, odgovor) {
    User.find().sort([['izposojenihTrenutno', 'descending']]).limit(1)
    .exec(function(napaka,objekt2){
        //console.log(JSON.stringify(objekt2, null, "    "));
        vrniJsonOdgovor(odgovor, 200, objekt2);
    });
};

module.exports.userTopIzposoj = function(zahteva, odgovor) {
    User.find().sort([['stIzposoj', 'descending']]).limit(1)
    .exec(function(napaka,objekt2){
        //console.log(JSON.stringify(objekt2, null, "    "));
        vrniJsonOdgovor(odgovor, 200, objekt2);
    });
};

module.exports.userCreate = function(zahteva, odgovor) {
  User.create({
    upIme:zahteva.body.upIme,
    ime:zahteva.body.ime,
    priimek:zahteva.body.priimek,
  }, function(napaka, obj) {
    if (napaka) {
      vrniJsonOdgovor(odgovor, 400, napaka);
    } else {
      vrniJsonOdgovor(odgovor, 201, obj);
    }
  });
};

module.exports.userDelete = function(zahteva, odgovor) {
  var id = zahteva.params.id;
  if (id) {
    User
      .findByIdAndRemove(id)
      .exec(
        function(napaka, obj) {
          if (napaka) {
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
          }
          vrniJsonOdgovor(odgovor, 204, null);
        }
      );
  } else {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": 
        "Ne najdem uporabnika, id je obvezen parameter."
    });
  }
};


/*--------------------------------- OSTALO --------------------------------- */


module.exports.dbZacetniPodatki = function(zahteva, odgovor) {
  Objekt.create(
    {
      naslov:"Pika Nogavička",
      avtor:"Astrid Lindgren",
      ocena:2,
      status:true,
      medij:"Knjiga",
      zanr:"Otroška Knjiga",
      datumIzdaje: "2012-12-12", 
      jezik:"Slovenščina",
      nIzposojena:25
    }, 
    {
      naslov:"Pot",
      avtor:"Zaplotnik Nejc",
      ocena:4,
      status:true,
      medij:"Knjiga",
      zanr:"dokumentarna literatura",
      datumIzdaje: "1970-01-01",
      jezik:"Slovenščina",
      nIzposojena:20
    }, 
    {
      naslov:"Zlata mačja preja",
      avtor:"Makarovič, Svetlana",
      ocena:4,
      status:true,
      medij:"Knjiga",
      zanr:"razne literarne vrste",
      datumIzdaje: "2018-12-12",
      jezik:"Slovenščina",
      nIzposojena:5
    }, 
    {  
      naslov:"Pippi Longstocking",
      avtor:"Astrid Lindgren",
      ocena:2,
      status:true,
      medij:"Knjiga",
      zanr:"Otroška Knjiga",
      datumIzdaje: "1945-11-12",
      jezik:"Angleščina",
      nIzposojena:100,
      gBookId:"Xe8otKSjdX0C"
    }, 
    {  
      naslov:"1984",
      avtor:"George Orwell",
      ocena:4,
      status:true,
      medij:"Knjiga",
      zanr:"Satira",
      datumIzdaje: "1949-06-08",
      jezik:"Angleščina",
      nIzposojena:250,
      gBookId:"kotPYEqx7kMC",
      komentarji:
        [  
          {  
             avtor:"Janez Novak",
             besediloKomentarja:"Odlična knjiga, takle mamo!"
          }
        ]
    }, 
    {  
      naslov:"Animal Farm",
      avtor:"George Orwell",
      ocena:4,
      status:false,
      medij:"Knjiga",
      zanr:"Satira",
      datumIzdaje: "1945-08-17",
      jezik:"Angleščina",
      nIzposojena:34,
      gBookId:"nkalO3OsoeMC"
    }
  , function(napaka, obj) {
    if (napaka) {
      vrniJsonOdgovor(odgovor, 400, napaka);
    } else {
        User.create(
          { 
          	upIme : "jnovak", 
          	ime : "Janez", 
          	priimek : "Novak", 
          	izposojenihTrenutno : 3, 
          	stIzposoj : 20
          }, 
          { 
            upIme : "mkranjc", 
            ime : "Metka", 
            priimek : "Kranjc", 
            izposojenihTrenutno : 1, 
            stIzposoj : 45 
          }
        , function(napaka, obj) {
          if (napaka) {
            vrniJsonOdgovor(odgovor, 400, napaka);
          } else {
            vrniJsonOdgovor(odgovor, 201, obj);
          }
        });
      }
  });
};

module.exports.dbIzbrisi = function(zahteva, odgovor) {
  Objekt
    .remove({}
    , function(napaka, obj) {
    if (napaka) {
      vrniJsonOdgovor(odgovor, 400, napaka);
    } else {
        User
          .remove({}
          , function(napaka, obj) {
          if (napaka) {
            vrniJsonOdgovor(odgovor, 400, napaka);
          } else {
              Izposoja
                .remove({}
                , function(napaka, obj) {
                if (napaka) {
                  vrniJsonOdgovor(odgovor, 400, napaka);
                } else {
                  vrniJsonOdgovor(odgovor, 201, obj);
                }
              });
          }
        });
    }
  });
};

module.exports.rezervacija = function(zahteva, odgovor) {
  
  Izposoja.create(
    {
      uporabnikId:zahteva.body.uporabnikId,
      knjigaId:zahteva.body.knjigaId,
    }
  , function(napaka, obj) {
    if (napaka) {
      vrniJsonOdgovor(odgovor, 400, napaka);
    } else {
      vrniJsonOdgovor(odgovor, 201, obj);
    }
  });
};