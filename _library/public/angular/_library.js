/*global angular, $http*/
var _libraryApp = angular.module('_library', [])
//-----------------------------------------------------------PODATKI---------------------------------------------------------------
var _libraryObjektiPodatki = function($http) {
    return $http.get('api/objekti')
};


//------------------------------------------------------KONTROLERJI ANGULAR----------------------------------
var seznamObjektiCtrl = function($scope, _libraryObjektiPodatki) {
    console.log("seznamObjektiCTRL");
    $scope.sporocilo = "Iščem podatke za prikazovanje knjig.";
    _libraryObjektiPodatki.then(
        function success(odgovor) {
            $scope.sporocilo = odgovor.data.length > 0 ? "" : "Ne najdem knjig.";
            $scope.data = {
                objekti: odgovor.data
            };
        },
        function error(odgovor) {
            $scope.sporocilo = "Prišlo je do napake!";
            console.log(odgovor.e);
        }
    );
    
    $scope.show_object_detailUser=function(){//TODO
        console.log("treba bo dopolnit angular/_library.js controller seznamDomovUserCtrl. trenutne se neda ker se stran izvaja z pug predlogami in je totalna bolecina");
    }
};

//------------------------------------------------------------MIXIN??---------------------------------------------
var prikaziOceno = function() {//klicat moramo prikazi-oceno ker se vse pretvori v male crke in camel case se doda vezaj
  return {
          scope: {
      trenutnaOcena: "=ocena"
    },
    templateUrl: "/angular/ocena-zvezdice.html"
  };
};

var prikaziIzposojeno = function(){
      return {
          scope: {
      status: "=izposojeno"
    },
    templateUrl: "/angular/izposoja.html"
  };
};




_libraryApp
    .controller('seznamObjektiCtrl', seznamObjektiCtrl)
    .directive('prikaziOceno', prikaziOceno)
    .directive('prikaziIzposojeno',prikaziIzposojeno)
    .service('_libraryObjektiPodatki', _libraryObjektiPodatki);
    
