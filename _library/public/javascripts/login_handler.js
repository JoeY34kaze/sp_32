
function handle_login(){
    var email = document.getElementById('input_email');
    var pass = document.getElementById('input_password');
    console.log(email.value);
    console.log(pass.value);
    
    if(email.value=="user@user"  && pass.value=="user"){
        //redirect to user site
        window.location.href="/domovUser";
    }else if(email.value=="admin@admin"  && pass.value=="admin"){
        // redirect to admin site
        window.location.href="/domovAdmin";
    }else { //error
        highlight(email);
        highlight(pass);
    }
}

function handle_registration(event){
    event.preventDefault();
    var pass = document.getElementById('input_password');
    var rePass = document.getElementById('input_re-password');
    console.log(pass.value);
    console.log(rePass.value);
    
    if(pass.value===rePass.value){
        window.location.href="/";
    } else {
        highlight(pass);
        highlight(rePass);
    }
}

function highlight(obj) {
    var orig = obj.style.backgroundColor;
    obj.style.backgroundColor = 'red';
    setTimeout(function() {
        obj.style.backgroundColor = orig;
    }, 3000);
}

//var btn_login="#btn_login";
//$(btn_login).click(function(){
// handle_login();
//});