function show_object_detailUser(id){//kot parameter dobi id objekta na katrega je uporabnik kliknil na pogledu domov.
    console.log(id);
    window.location.href="/detailUser/"+id;
}

function show_object_detailAdmin(id){//kot parameter dobi id objekta na katrega je uporabnik kliknil na pogledu domov.
    //console.log(id);
    window.location.href="/detailAdmin/"+id;
}


function delete_book(id){
    window.location.href="/izbrisiAdmin/"+id;
}

function edit_book(id){
    window.location.href="/urejanjeElementaAdmin/" + id;
}

function redirectTogoogleBook(gbId){
    console.log(gbId);
    window.location.href="/googleBook/"+gbId;
}

function removeComment(id, commId){
    console.log(id);
    window.location.href="/detailAdmin/"+id+"/komentar/"+commId+"/izbrisi";
}

function vnesiZacetnePodatke(){
    window.location.href="/dbZacetniPodatki";
}

function izbrisiBazo(){
    window.location.href="/dbIzbrisi";
}

function makeReservation(idKnjige, idUporabnika = "jnovak"){ //jnovak je samo začasno, potrebno je spremeniti v dejanskega uporabnika (njegov ID, ne pa up. imena). To bomo naredili ko bomo implementirali prijavo.
    window.location.href='/rezervacija/'+idKnjige+'/'+idUporabnika;
}