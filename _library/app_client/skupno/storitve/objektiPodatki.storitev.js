var objektiPodatki = function($http) {
  var objekti = function() {
    return $http.get('api/objekti');
  };
  
  var objektPodrobnosti = function(id) {
    return $http.get('api/objekti/'+id);
  }
  
  return {
    objekti: objekti,
    objektPodrobnosti: objektPodrobnosti,
  };
};

/* global _libraryApp */
_libraryApp
  .service('objektiPodatki', objektiPodatki);