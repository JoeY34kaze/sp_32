function objektiCtrl($scope, objektiPodatki) {
  var vm=this;
  vm.glavaStrani = {
    naslov: 'Knjiznica',
    podnaslov: 'Super knjige za vsakogar!'
  };
  
  vm.sporocilo = "Iščem podatke za prikazovanje knjig.";
  console.log("objektiCtrl");
  
  vm.sporocilo= "Pridobivam podatke.."
  console.log("Pridobivam Podatke");
  objektiPodatki.objekti().then(
    function success(odgovor){
      console.log("Uspesno pridobil podatke");
      vm.sporocilo = odgovor.data.length > 0 ? "" : "Ne najdem knjig.";
      vm.data = {
        objekti: odgovor.data
      };
      console.log(vm.data);
    },
    function error(odgovor) {
      console.log("error pridobivanja podatkov");
      vm.sporocilo = "Prišlo je do napake!";
      console.log(odgovor.e);
    }
  );
      
  $scope.show_object_detailUser = function(id){//TODO
    window.location.href = "/#!/detail/" + id
    console.log(id);
  }
  
}



/* global _libraryApp */
_libraryApp
  .controller('objektiCtrl', objektiCtrl);