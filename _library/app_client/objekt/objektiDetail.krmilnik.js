function objektiDetailCtrl($routeParams, objektiPodatki) {
  var vm=this;
  vm.glavaStrani = {
    naslov: 'Knjiznica',
    podnaslov: 'Super knjige za vsakogar!'
  };
  
  vm.sporocilo = "Iščem podatke za prikazovanje knjige.";
   console.log("objektiDetailCtrl");
  
  vm.sporocilo= "Pridobivam podatke.."
  console.log("Pridobivam Podatke");
  objektiPodatki.objektPodrobnosti($routeParams.id).then(
    function success(odgovor){
      console.log("Uspesno pridobil podatke");
      vm.sporocilo = odgovor.data_id ? "" : "Ne najdem knjige.";
      vm.data = {
        objekt: odgovor.data
      };
      console.log(vm.data);
    },
    function error(odgovor) {
      console.log("error pridobivanja podatkov");
      vm.sporocilo = "Prišlo je do napake!";
      console.log(odgovor.e);
    }
  );
}



/* global _libraryApp */
_libraryApp
  .controller('objektiDetailCtrl', objektiDetailCtrl);