/* global angular */
var _libraryApp = angular.module('_library', ['ngRoute']);

/*
Dodajanje krmilnika usmerjevalniku je dokaj preprost proces z naslednjimi koraki:

1 kreiranje krmilnika v lastni datoteki,
2 dodajanje krmilnika v Angular aplikacijo,
3 opredelitev krmilnika v metodi nastavitev znotraj usmerjevalnika,
4 vključitev datoteke za brskalnik.
*/
function nastavitev($routeProvider) {//-----------usmerjevalnik za spa -------------
  $routeProvider
    .when('/', {
        templateUrl: 'objekt/objekti.pogled.html',
        controller: 'objektiCtrl',
        controllerAs: 'vm'
    })
    .when('/detail/:id',{
      templateUrl: 'objekt/objektiDetail.pogled.html',
      controller: 'objektiDetailCtrl',
      controllerAs: 'vm'
    })
    .otherwise({redirectTo: '/'});
}

_libraryApp
  .config(['$routeProvider', nastavitev]);